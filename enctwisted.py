import enum
import logging
import os
import signal
import time
import uuid

from logging.handlers import RotatingFileHandler

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.asymmetric import padding, rsa

from twisted.internet import reactor
from twisted.internet.endpoints import connectProtocol, TCP4ClientEndpoint, TCP4ServerEndpoint
from twisted.internet.protocol import Factory, Protocol


logger = logging.getLogger(__name__)


def set_root_logger():
    root_logger = logging.getLogger()
    handler = RotatingFileHandler("cheshire.log", maxBytes=10000000, backupCount=5)
    formatter = logging.Formatter(
        "%(asctime)s-%(levelname)s-[%(threadName)s]:%(filename)s:%(funcName)s:%(lineno)d: %(message)s"
    )
    handler.setFormatter(formatter)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    root_logger.addHandler(console_handler)
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.DEBUG)


class Peer(object):

    def __init__(self, ip, port, protocol):
        self.ip = ip
        self.port = port
        self.protocol = protocol
        self.id = None

    def __eq__(self, other):
        if self.__class__ == other.__class__:
            return self.ip == other.ip
        return NotImplemented

    def __ne__(self, other):
        if self.__class__ == other.__class__:
            return self.ip != other.ip
        return NotImplemented

    def __str__(self):
        return f"{self.ip}:{self.port}"

    def __format__(self, format_spec):
        return f"Peer({self.ip}, {self.port}, {self.protocol})"


class CheshireState(enum.IntEnum):
    STARTUP = 0
    PUBLIC = 1
    ESTABLISHED = 2
    # SYMETTRIC = 2
    # ESTABLISHED = 3


class CheshireProtocol(Protocol):

    def __init__(self, factory):
        self.factory = factory
        self.id = self.factory.id
        self.host = None
        self.peer = None
        self.state = CheshireState.STARTUP
        self.last_heart = None
        self.sk = None
        self.pk = None

    def set_state(self, state):
        logger.debug(f"State Change: {self.state.name} -> {state.name}")
        self.state = state

    def state_handler(self, message):
        if self.state == CheshireState.STARTUP:
            self.handle_public_key(message)
        elif self.state == CheshireState.PUBLIC:
            self.handle_encryption_key(message)
        else:
            logger.debug(f"UNHANDLED STATE {self.state.name}")

    def define_hosts(self):
        host = self.transport.getHost()
        peer = self.transport.getPeer()
        self.host = Peer(host.host, host.port, host.type)
        self.peer = Peer(peer.host, peer.port, peer.type)

    def connectionMade(self):
        self.define_hosts()
        if self.host == self.peer:
            self.transport.loseConnection()
        elif self.state == CheshireState.STARTUP:
            logger.info(f"New {self.peer.protocol} connection from {self.peer.ip}")
        else:
            logger.error(f"CONNECTION ESTABLISHED ON BAD STATE {self.state}")

    def connectionLost(self, reason):
        if self.peer.id in self.factory.peers:
            self.factory.peers.pop(self.peer.id)
            self.set_state(CheshireState.STARTUP)
            logger.info(f"Lost {self.peer.protocol} connection from {self.peer}: {reason}")

    def dataReceived(self, data):
        logger.debug(data)
        self.state_handler(data)

    def write_message(self, message):
        if not isinstance(message, bytes):
            logger.debug("Encoding message to bytes")
            enc_message = message.encode("utf-8")
        else:
            logger.debug("Message was already bytes")
            enc_message = message
        self.transport.write(enc_message)

    def load_message(self, message):
        logger.debug(f"Message received {message}")
        return message.decode("utf-8")

    def encrypt_message(self, message):
        return self.pk.encrypt(
            message,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA512()),
                algorithm=hashes.SHA512(),
                label=None
            )
        )

    def decrypt_message(self, message):
        if not self.sk:
            self.sk = load_private_key()
        return self.sk.decrypt(
            message,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA512()),
                algorithm=hashes.SHA512(),
                label=None
            )
        )

    def send_public_key(self):
        pk = load_public_key_text()
        self.write_message(pk)
        self.set_state(CheshireState.PUBLIC)

    def send_encryption_key(self):
        ki = self.key + self.iv
        ki_encrypted = self.encrypt_message(ki)
        self.write_message(ki_encrypted)
        self.set_state(CheshireState.ESTABLISHED)

    def handle_public_key(self, message):
        self.pk = load_public_key_from_bytes(message)
        self.generate_encryption_key()
        self.send_encryption_key()

    def handle_encryption_key(self, message):
        msg = self.decrypt_message(message)
        self.key = msg[:32]
        self.iv = msg[-16:]
        self.cipher = Cipher(algorithms.AES(self.key), modes.CBC(self.iv), default_backend())
        logger.debug(f"key {self.key}")
        logger.debug(f"iv {self.iv}")
        self.set_state(CheshireState.ESTABLISHED)

    def generate_encryption_key(self):
        self.key = os.urandom(32)
        self.iv = os.urandom(16)
        logger.debug(f"key {self.key}")
        logger.debug(f"iv {self.iv}")
        self.cipher = Cipher(algorithms.AES(self.key), modes.CBC(self.iv), default_backend())


class CheshireFactory(Factory):

    def __init__(self):
        self.peers = {}
        self.id = str(uuid.uuid4())

    def buildProtocol(self, address):
        return CheshireProtocol(self)


def gotProtocol(protocol):
    protocol.send_public_key()


def endpoint_connect(host, port, protocol):
    logger.debug(f"Attempting new connection to {host}:{port}")
    client = TCP4ClientEndpoint(reactor, host, port)
    d = connectProtocol(client, protocol)
    d.addCallback(gotProtocol)


def sig_handle(signum, frame):
    logger.critical(f"Exiting program immediately signum={signum}, frame={frame}")
    reactor.stop()
    os._exit(0)


def load_public_key():
    with open("pk.pem", "rb") as pkin:
        pk_pem = pkin.read()
    pk = serialization.load_pem_public_key(pk_pem, default_backend())
    return pk_pem


def load_private_key():
    with open("sk.pem", "rb") as skin:
        sk_pem = skin.read()
    sk = serialization.load_pem_private_key(sk_pem, None, default_backend())
    return sk


def load_public_key_text():
    with open("pk.pem", "r") as pkin:
        pk_pem = pkin.read()
    return pk_pem


def load_public_key_from_bytes(pk_bytes):
    pk = serialization.load_pem_public_key(pk_bytes, default_backend())
    return pk


def load_public_key_from_str(pk_str):
    pk_bytes = pk_str.encode("utf-8")
    pk = serialization.load_pem_public_key(pk_bytes, default_backend())
    return pk


def key_exists(name):
    return os.path.exists(name)


def certificate_init():
    if key_exists("sk.pem") and key_exists("pk.pem"):
        return True

    sk = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )

    pk = sk.public_key()

    spem = sk.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption()
    )
    with open("sk.pem", "wb+") as skfile:
        skfile.write(spem)

    ppem = pk.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    with open("pk.pem", "wb+") as pkfile:
        pkfile.write(ppem)



if __name__ == "__main__":
    signal.signal(signal.SIGTERM, sig_handle)
    signal.signal(signal.SIGINT, sig_handle)
    set_root_logger()
    BOOTSTRAP_LIST = [
        ("192.168.1.204", 1776),
        # ("192.168.1.77", 1776),
        ("192.168.1.79", 1776),
    ]

    certificate_init()
    myfactory = CheshireFactory()
    server = TCP4ServerEndpoint(reactor, 1776)
    server.listen(myfactory)

    for host, port in BOOTSTRAP_LIST:
        endpoint_connect(host, port, CheshireProtocol(myfactory))

    logger.debug("Reactor Startup")
    reactor.run(installSignalHandlers=False)